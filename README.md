# Implementation-of-rLEDBAT-in-ns-3


### Overview

rLEDBAT, a receiver-based, less-than-best-effort congestion control algorithm. rLEDBAT is inspired by
LEDBAT but it is implemented in the TCP receiver and controls the sending rate of the sender through the TCP Receiver Window.

### Working

* rLEDBAT uses the Receive Window (RCV.WND) of TCP to enable the receiver to control the sender's rate.

* RCV.WND is used to announce the available receive buffer to the sender for flow control purposes.

* fc.WND - flow Control Reciever Window as proposed by RFCRFC793bis (standard TCP receiver window).

* rl.WND = Reciver window as proposed by rLEDBAT draft(this needs to be calculated using the LEDBAT equation replacing the one-way delay by RTT delay).
```
Thus
RCV.WND = min(fl.WND, rl.WND)
```
```
SND.WND = min(cwnd , RCV.WND) = min(cwnd, fl.WND, rl.WND)
```

### Running rLEDBAT

In the CLI, from the ns3 main directory, run the following commands:
```
./ns3 configure --enable-examples --enable-tests
```

```
./ns3 build
```

After building ns3, make sure tcpVariant is set to "TcpRLedbat" in src/examples/wireless/wifi-tcp.cc. Run this example by 
```
./ns3 run wifi-tcp
```
If final average throughput is greater than 50, then the algorithm works fine!

