#include "tcp-rledbat.h"
#include "ns3/log.h"

namespace ns3{

NS_LOG_COMPONENT_DEFINE("TcpRLedbat");
NS_OBJECT_ENSURE_REGISTERED(TcpRLedbat);


TypeId
TcpRLedbat::GetTypeId(){
    static TypeId tid =
        TypeId("ns3::TcpRLedbat")
            .SetParent<TcpNewReno>()
            .AddConstructor<TcpRLedbat>()
            .SetGroupName("Internet");
            
    return tid;
}

TcpRLedbat::TcpRLedbat()
    : TcpNewReno()
{
    NS_LOG_FUNCTION(this);
    m_target = MilliSeconds(100);
    m_gain = 1;
    m_baseHistoLen = 10;
    m_noiseFilterLen = 4;
    InitCircBuf(m_baseHistory);
    InitCircBuf(m_noiseFilter);
    m_lastRollover = 0;
    m_sndCwndCnt = 0;
    m_flag = LEDBAT_CAN_SS;
    m_minCwnd = 2;
};

TcpRLedbat::TcpRLedbat(const TcpRLedbat& sock)
    : TcpNewReno(sock)
{
    NS_LOG_FUNCTION(this);
    m_target = sock.m_target;
    m_gain = sock.m_gain;
    //m_doSs = sock.m_doSs;
    m_baseHistoLen = sock.m_baseHistoLen;
    m_noiseFilterLen = sock.m_noiseFilterLen;
    m_baseHistory = sock.m_baseHistory;
    m_noiseFilter = sock.m_noiseFilter;
    m_lastRollover = sock.m_lastRollover;
    m_sndCwndCnt = sock.m_sndCwndCnt;
    m_flag = sock.m_flag;
    m_minCwnd = sock.m_minCwnd;
}

void
TcpRLedbat::IncreaseWindow(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked)
{
    //NS_LOG_FUNCTION(this << tcb << segmentsAcked);

    if (tcb->m_cWnd < tcb->m_ssThresh)
    {
        SlowStart(tcb, segmentsAcked);
    }

    if (tcb->m_cWnd >= tcb->m_ssThresh)
    {
        //std::cout << "Hi!" << std::endl;
        CongestionAvoidance(tcb, segmentsAcked);
    }
}

void
TcpRLedbat::CongestionAvoidance(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked){
    uint32_t rcv_WND;
    uint32_t cwnd;

    if (segmentsAcked > 0)
    {
        double adder =
            static_cast<double>(tcb->m_segmentSize * tcb->m_segmentSize) / tcb->m_cWnd.Get();
        adder = std::max(1.0, adder);
        cwnd = tcb->m_cWnd + static_cast<uint32_t>(adder);
    }

    if ((m_flag & LEDBAT_VALID_OWD) == 0)
    {
        tcb->m_cWnd = cwnd;
        return;
    }

    rcv_WND = getReceiverWindow(tcb, segmentsAcked);
    //std::cout << "Rcv wnd: " << rcv_WND;
    //std::cout << " cwnd: "<< cwnd << std::endl;

    if(rcv_WND < cwnd && tcb->m_cWnd - rcv_WND > tcb->m_segmentSize){
        tcb->m_cWnd -= tcb->m_segmentSize;
    }
    else{
        tcb->m_cWnd = std::min(rcv_WND, cwnd);
    }

    //std::cout << "cwnd: "<< tcb->m_cWnd << std::endl;
}

uint32_t
TcpRLedbat::getReceiverWindow(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked){

   int64_t queue_delay;
   double offset;
   uint32_t cwnd = (tcb->m_cWnd.Get());
   uint32_t max_cwnd;
   uint64_t current_delay = CurrentDelay(&TcpRLedbat::MinCircBuf);
   uint64_t base_delay = BaseDelay();

   if (current_delay > base_delay)
   {
       queue_delay = static_cast<int64_t>(current_delay - base_delay);
       offset = m_target.GetMilliSeconds() - queue_delay;
   }
   else
   {
       queue_delay = static_cast<int64_t>(base_delay - current_delay);
       offset = m_target.GetMilliSeconds() + queue_delay;
   }
   offset *= m_gain;
   m_sndCwndCnt = static_cast<int32_t>(offset * segmentsAcked * tcb->m_segmentSize);
   double inc = (m_sndCwndCnt * 1.0) / (m_target.GetMilliSeconds() * tcb->m_cWnd.Get());
   cwnd += (inc * tcb->m_segmentSize);

   max_cwnd = static_cast<uint32_t>(tcb->m_highTxMark.Get() - tcb->m_lastAckedSeq) +
              segmentsAcked * tcb->m_segmentSize;
   cwnd = std::min(cwnd, max_cwnd);
   cwnd = std::max(cwnd, m_minCwnd * tcb->m_segmentSize);

   return cwnd;
}

void
TcpRLedbat::InitCircBuf(OwdCircBuf& buffer)
{   NS_LOG_FUNCTION(this);
    buffer.buffer.clear();
    buffer.min = 0;
}

uint32_t
TcpRLedbat::MinCircBuf(OwdCircBuf& b)
{
     NS_LOG_FUNCTION_NOARGS();
    if (b.buffer.empty())
    {
        return ~0U;
    }
    else
    {
        return b.buffer[b.min];
    }
}

uint32_t
TcpRLedbat::CurrentDelay(FilterFunction filter)
{
    NS_LOG_FUNCTION(this);
    return filter(m_noiseFilter);
}

uint32_t
TcpRLedbat::BaseDelay()
{
    NS_LOG_FUNCTION(this);
    return MinCircBuf(m_baseHistory);
}

void
TcpRLedbat::AddDelay(OwdCircBuf& cb, uint32_t owd, uint32_t maxlen){
    NS_LOG_FUNCTION(this << owd << maxlen << cb.buffer.size());
    if (cb.buffer.empty())
    {
        NS_LOG_LOGIC("First Value for queue");
        cb.buffer.push_back(owd);
        cb.min = 0;
        return;
    }
    cb.buffer.push_back(owd);
    if (cb.buffer[cb.min] > owd)
    {
        cb.min = static_cast<uint32_t>(cb.buffer.size() - 1);
    }
    if (cb.buffer.size() >= maxlen)
    {
        NS_LOG_LOGIC("Queue full" << maxlen);
        cb.buffer.erase(cb.buffer.begin());
        cb.min = 0;
        NS_LOG_LOGIC("Current min element" << cb.buffer[cb.min]);
        for (uint32_t i = 1; i < maxlen - 1; i++)
        {
            if (cb.buffer[i] < cb.buffer[cb.min])
            {
                cb.min = i;
            }
        }
    }
}

void TcpRLedbat::UpdateBaseDelay(uint32_t owd){
    NS_LOG_FUNCTION(this << owd);
    if (m_baseHistory.buffer.empty())
    {
        AddDelay(m_baseHistory, owd, m_baseHistoLen);
        return;
    }
    uint64_t timestamp = static_cast<uint64_t>(Simulator::Now().GetSeconds());

    if (timestamp - m_lastRollover > 60)
    {
        m_lastRollover = timestamp;
        AddDelay(m_baseHistory, owd, m_baseHistoLen);
    }
    else
    {
        auto last = static_cast<uint32_t>(m_baseHistory.buffer.size() - 1);
        if (owd < m_baseHistory.buffer[last])
        {
            m_baseHistory.buffer[last] = owd;
            if (owd < m_baseHistory.buffer[m_baseHistory.min])
            {
                m_baseHistory.min = last;
            }
        }
    }
}

void
TcpRLedbat::PktsAcked(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked, const Time& rtt)
{
    NS_LOG_FUNCTION(this << tcb << segmentsAcked << rtt);
    if (tcb->m_rcvTimestampValue == 0 || tcb->m_rcvTimestampEchoReply == 0)
    {
        m_flag &= ~LEDBAT_VALID_OWD;
    }
    else
    {
        m_flag |= LEDBAT_VALID_OWD;
    }
    if (rtt.IsPositive())
    {
        AddDelay(m_noiseFilter,
                 tcb->m_rcvTimestampValue - tcb->m_rcvTimestampEchoReply,
                 m_noiseFilterLen);
        UpdateBaseDelay(tcb->m_rcvTimestampValue - tcb->m_rcvTimestampEchoReply);
    }
}

}
