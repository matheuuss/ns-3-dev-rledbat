#ifndef TCP_RLEDBAT_H
#define TCP_RLEDBAT_H

#include "tcp-congestion-ops.h"
#include "tcp-ledbat.h"

#include "ns3/log.h"
#include "ns3/simulator.h" // Now ()
#include <vector>

namespace ns3{

    class TcpRLedbat: public TcpNewReno{
        private:
            enum State : uint32_t
                {
                    LEDBAT_VALID_OWD = (1 << 1), //!< If valid timestamps are present
                    LEDBAT_CAN_SS = (1 << 3)     //!< If LEDBAT allows Slow Start
                };

        public:
            static TypeId GetTypeId();

            TcpRLedbat();

            TcpRLedbat(const TcpRLedbat& sock);

            void
            IncreaseWindow(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked) override;

            void
            PktsAcked(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked, const Time& rtt) override;

        protected:
            /**
             * \brief Reduce Congestion
             *
             * \param tcb internal congestion state
             * \param segmentsAcked count of segments ACKed
             */
            void CongestionAvoidance(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked) override;

        private:
            /**
             *\brief Buffer structure to store delays
             */
            uint32_t
            getReceiverWindow(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked);

            struct OwdCircBuf
            {
                std::vector<uint32_t> buffer; //!< Vector to store the delay
                uint32_t min;                 //!< The index of minimum value
            };

            /**
             * \brief Initialise a new buffer
             *
             * \param buffer The buffer to be initialised
             */
            void InitCircBuf(OwdCircBuf& buffer);

            /// Filter function used by LEDBAT for current delay
            typedef uint32_t (*FilterFunction)(OwdCircBuf&);

            /**
             * \brief Return the minimum delay of the buffer
             *
             * \param b The buffer
             * \return The minimum delay
             */
            static uint32_t MinCircBuf(OwdCircBuf& b);

            /**
             * \brief Return the value of current delay
             *
             * \param filter The filter function
             * \return The current delay
             */
            uint32_t CurrentDelay(FilterFunction filter);

            /**
             * \brief Return the value of base delay
             *
             * \return The base delay
             */
            uint32_t BaseDelay();

            /**
             * \brief Add new delay to the buffers
             *
             * \param cb The buffer
             * \param owd The new delay
             * \param maxlen The maximum permitted length
             */
            void AddDelay(OwdCircBuf& cb, uint32_t owd, uint32_t maxlen);

            /**
             * \brief Update the base delay buffer
             *
             * \param owd The delay
             */
            void UpdateBaseDelay(uint32_t owd);

            Time m_target;             //!< Target Queue Delay
            double m_gain;             //!< GAIN value from RFC
            //SlowStartType m_doSs;      //!< Permissible Slow Start State
            uint32_t m_baseHistoLen;   //!< Length of base delay history buffer
            uint32_t m_noiseFilterLen; //!< Length of current delay buffer
            uint64_t m_lastRollover;   //!< Timestamp of last added delay
            int32_t m_sndCwndCnt;      //!< The congestion window addition parameter
            OwdCircBuf m_baseHistory;  //!< Buffer to store the base delay
            OwdCircBuf m_noiseFilter;  //!< Buffer to store the current delay
            uint32_t m_flag;           //!< LEDBAT Flag
            uint32_t m_minCwnd;

    };

}

#endif